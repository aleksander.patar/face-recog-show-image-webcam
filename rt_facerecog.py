import hnswlib
import numpy as np
import cv2
import face_recognition

# RFB setup
from src_ultra.vision.ssd.config.fd_config import define_img_size
input_img_size = 320
define_img_size(input_img_size)
from src_ultra.vision.ssd.mb_tiny_RFB_fd import create_Mb_Tiny_RFB_fd, create_Mb_Tiny_RFB_fd_predictor
label_path = "./src_ultra/models/voc-model-labels.txt"
class_names = [name.strip() for name in open(label_path).readlines()]
num_classes = len(class_names)
test_device = "cpu"
candidate_size = 10000
threshold = 0.999
model_path = "src_ultra/models/pretrained/version-RFB-320.pth"
# model_path = "src_ultra/models/pretrained/version-RFB-640.pth"
net = create_Mb_Tiny_RFB_fd(len(class_names), is_test=True, device=test_device)
predictor = create_Mb_Tiny_RFB_fd_predictor(net, candidate_size=candidate_size, device=test_device)
net.load(model_path)
#RFB setup end

with open("filenames.txt", "r") as file:
    readname = eval(file.readline())
index = hnswlib.Index(space='cosine', dim=128)
index.load_index("index.bin")

threshold = 0.061018319674191926 #threshold dlib

cap = cv2.VideoCapture(0)

while(True):
    # Capture frame-by-frame
    ret, camframe = cap.read()
    if camframe is None:
        print("camfame is None, end program")
        break

    inputimage = cv2.cvtColor(camframe, cv2.COLOR_BGR2RGB)
    boxes, labels, probs = predictor.predict(inputimage, candidate_size / 2, threshold)

    for i in range(boxes.size(0)):
        box = boxes[i, :]
        box=list(map(int,box))
        r=box[0]
        o=box[1]
        q=box[2]
        p=box[3]
        v=int((3*r+o+q-p)/4)
        s=int((r+3*o-q+p)/4)
        u=int((r-o+3*q+p)/4)
        t=int((-r+o+q+3*p)/4)
        crop_img = inputimage[s:t, v:u]
        known_face_locations = [(0,u-v,t-s,0)]
        face_encoding=face_recognition.face_encodings(crop_img, known_face_locations, num_jitters=1, model="small")
        if len(face_encoding) == 0:
            name="cannot_encode"
            cv2.rectangle(camframe, (v, s), (u, t), (0, 0, 255), 4)
            cv2.putText(camframe, name,
                (box[0], box[1] - 10),
                cv2.FONT_HERSHEY_SIMPLEX,
                0.5,  # font scale
                (0, 0, 255),
                2)  # line type
            break
        else:
            labeles, distances = index.knn_query(face_encoding, 1)
            distance=distances.item(0,0)
        if distance > threshold:
            name="unknown"
            cv2.rectangle(camframe, (v, s), (u, t), (0, 0, 255), 4)
            cv2.putText(camframe, name,
                (box[0], box[1] - 10),
                cv2.FONT_HERSHEY_SIMPLEX,
                0.5,  # font scale
                (0, 0, 255),
                2)  # line type
            break
        else:
            labele=labeles.item(0,0)
            name=readname[labele]
            cv2.rectangle(camframe, (v, s), (u, t), (0, 255, 0), 4)
            cv2.putText(camframe, name,
                (box[0], box[1] - 10),
                cv2.FONT_HERSHEY_SIMPLEX,
                0.5,  # font scale
                (0, 0, 255),
                2)  # line type
        
    # Display the resulting frame
    cv2.imshow('frame',camframe)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()