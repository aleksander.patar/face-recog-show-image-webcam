import numpy as np
import cv2
import hnswlib
import face_recognition
from os import walk

# RFB setup
print("initiating ultralight face detection")
from src_ultra.vision.ssd.config.fd_config import define_img_size
input_img_size = 320
define_img_size(input_img_size)
from src_ultra.vision.ssd.mb_tiny_RFB_fd import create_Mb_Tiny_RFB_fd, create_Mb_Tiny_RFB_fd_predictor
label_path = "./src_ultra/models/voc-model-labels.txt"
class_names = [name.strip() for name in open(label_path).readlines()]
num_classes = len(class_names)
test_device = "cpu"
candidate_size = 10000
threshold = 0.99
model_path = "src_ultra/models/pretrained/version-RFB-320.pth"
# model_path = "src_ultra/models/pretrained/version-RFB-640.pth"
net = create_Mb_Tiny_RFB_fd(len(class_names), is_test=True, device=test_device)
predictor = create_Mb_Tiny_RFB_fd_predictor(net, candidate_size=candidate_size, device=test_device)
net.load(model_path)
#RFB setup end

folderlocation='./datasetfoto/'
croppedlocation='./croppedfoto/'
_, _, filenames = next(walk(folderlocation))
encodings = []

for filename in filenames:
    img = cv2.imread(folderlocation+filename)
    inputimage = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    boxes, labels, probs = predictor.predict(inputimage, candidate_size / 2, threshold)
    probs=probs.tolist()
    best_detect_index=probs.index(max(probs))
    box = boxes[best_detect_index, :]
    box=list(map(int,box))
    r=box[0]
    o=box[1]
    q=box[2]
    p=box[3]
    v=int((3*r+o+q-p)/4)
    s=int((r+3*o-q+p)/4)
    u=int((r-o+3*q+p)/4)
    t=int((-r+o+q+3*p)/4)
    crop_img = inputimage[s:t, v:u]
    known_face_locations = [(0,u-v,t-s,0)]
    face_encoding=face_recognition.face_encodings(crop_img, known_face_locations, num_jitters=1, model="small")
    if len(face_encoding) == 0:
        print ("unidentified by dlib")
    encodings.append(face_encoding[0])

# with open("encoding.txt", "w") as file:
#     file.write(str(encodings))
with open("filenames.txt", "w") as file:
    file.write(str(filenames))

p = hnswlib.Index(space='cosine', dim=128)
p.init_index(max_elements=len(encodings), ef_construction=100, M=16)
p.set_ef(10)
p.set_num_threads(4)
p.add_items(encodings)
p.save_index("index.bin")