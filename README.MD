# Face Recog Show Image Webcam

Program ini bertujuan untuk identifikasi orang dengan wajahnya, dari webcam, dan menampilkannya di sebuah window.

## Inisialisasi

1. Install dependency
```bash
pip install -r req.txt
```
2. Download gambar dari google drive dan letakkan dalam folder "/datasetfoto". Jika belum, ganti nama setiap foto. Hal ini karena indexing wajah masih berdasarkan nama file dari foto .jpg.
3. Jalankan pretrain.py
```bash
python pretrain.py
```
4. Pastikan bahwa file "index.bin" dan "filenames.txt" terbentuk.

## Running di Webcam
Jalankan program untuk membaca webcam

```bash
python rt_facerecog.py
```

## Cara kerja
1. Face Detection dengan [ultra light](https://github.com/Linzaer/Ultra-Light-Fast-Generic-Face-Detector-1MB).
2. Face Recognition dengan [dlib](https://buildmedia.readthedocs.org/media/pdf/face-recognition/latest/face-recognition.pdf).
3. Matching KNN dengan [HNSWlib](https://github.com/nmslib/hnswlib)
4. Threshold berdasarkan Capstone Project oleh Patar, Thea, Lusy.